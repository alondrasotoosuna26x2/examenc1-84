const llamandoFetch=()=>{
    let ganancia=0;
    const url = "http://127.0.0.1:5500/html/servicio.json";
        fetch(url)
        .then(respuesta => respuesta.json())
        .then(data=>mostrarDatos(data))
    
const mostrarDatos=(data)=>{
    const res = document.getElementById("table");
    
    for(let item of data){
        res.innerHTML+= "<tr><td>" + item.codigo+"</td><td>"
            + item.idcliente+"</td><td>"
            + item.descripcion+"</td><td>"
            + item.preciovta+"</td><td>"
            + item.preciocompra+"</td><td>"
            + item.cantidad+"</td><td>"
            + ((item.preciovta * item.preciocompra)*item.cantidad)
            + "</td></tr>";
        ganancia = ganancia+((item.preciovta * item.preciocompra)*item.cantidad);
    }

     document.getElementById('ganancia').value = ganancia;
    }
}

document.getElementById('btnCargar').addEventListener('click', function(){
    llamandoFetch();
})
document.getElementById('btnLimpiar').addEventListener('click', function(){
    const r = document.getElementById("table");
    r.innerHTML="<th>Código</th><th>ID Cliente</th><th>Descripción</th><th>Cantidad</th><th>Precio Venta</th><th>Precio Compra</th><th>Ganancia</th>";
})